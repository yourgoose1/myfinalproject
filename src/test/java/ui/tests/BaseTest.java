package ui.tests;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeSuite;

import java.util.Map;

public class BaseTest {

    @BeforeSuite
    public void setRemote() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("selenoid:options", Map.of("enableVNC", true));
        Configuration.remote = "http://51.250.123.179:4444/wd/hub";
        Configuration.browserSize = "1920x1080";
        Configuration.browser = "chrome";
        Configuration.browserVersion = "112.0";
        Configuration.browserCapabilities = capabilities;
    }

}
