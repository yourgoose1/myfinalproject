package ui.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

public class DashboardTest extends BaseTest implements PageObjectSupplier {

    @BeforeClass
    public void login() {
        loginPage().loginByAdmin();
    }

    @Test(description = "Проверка дрилдауна в раздел Leave List из панели быстрого доступа")
    public void checkDrilldownAtQuickPanel1() {
        dashboardPage().drilldownAtQuickPanel("Leave List", "Leave");

    }

    @Test(description = "Проверка дрилдауна в раздел Time из панели быстрого доступа")
    public void checkDrilldownAtQuickPanel2() {
        dashboardPage().drilldownAtQuickPanel("Timesheets", "Time");
    }

    @Test(description = "Проверка поиска в боковой панели по существующему разделу")
    public void checkSearching() {
        dashboardPage().checkSearching("Dire", "Directory");
        dashboardPage().setSideMenuItem("Directory", "Directory");
    }

    @Test(description = "Проверка поиска в боковой панели по несуществующему разделу")
    public void negativeSearching() {
        dashboardPage().checkSearching("BLABLA", "");
    }

    @Test(description = "Проверка появления поп-апа об успешном изменении")
    public void checkSuccessPopUp() {
        dashboardPage().configToolClick();
        dashboardPage().checkBoxClick();
        dashboardPage().saveBtnClick();
        dashboardPage().successPopUP();
    }

    @Test(description = "Проверка того, что поп-ап об изменениях не появится, если отменить изменени")
    public void checkSuccessPopUpHidden() {
        dashboardPage().configToolClick();
        dashboardPage().checkBoxClick();
        dashboardPage().cancelBtnClick();
        dashboardPage().successPopUpHidden();
    }

}