package ui.tests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

import static ui.tests.SystemUser.ADMIN;
import static ui.tests.SystemUser.NONVALIDUSER;

public class LoginPageTest extends BaseTest implements PageObjectSupplier {

    @Test(description = "Проверка логина с валидными кредами")
    public void loginPositive() {
        loginPage().openWebsite();
        loginPage().authorization(ADMIN.username, ADMIN.password);
        loginPage().clickLoginBtn();
        dashboardPage().checkSidePanel();
    }

    @Test(description = "Проверка логина с невалидными кредами")
    public void loginNegative() {
        loginPage().openWebsite();
        loginPage().authorization(NONVALIDUSER.username, NONVALIDUSER.password);
        loginPage().clickLoginBtn();
        loginPage().checkAlert();
    }

    @Test(description = "Проверка логина не указывая креды")
    public void loginWithoutCredentials() {
        loginPage().openWebsite();
        loginPage().clickLoginBtn();
        loginPage().checkRequiredLabelAlert();
        Selenide.sleep(3000);

    }
}
