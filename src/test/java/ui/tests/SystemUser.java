package ui.tests;

public enum SystemUser {
    ADMIN("Admin", "admin123"),
    NONVALIDUSER("odmen", "meme");

    SystemUser(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String username;
    public String password;
}
