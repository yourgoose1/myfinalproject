package ui.tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

public class PimTest extends BaseTest implements PageObjectSupplier {

    @BeforeClass
    public void login() {
        loginPage().loginByAdmin();
    }

    @Test(description = "Добавление нового рабочего")
    public void addNewEmployee() {
        pimpage().openPIM();
        pimpage().addBtnClick();
        pimpage().setFullName("Vladislav", "Neponyatnovich");
        dashboardPage().saveBtnClick();
        dashboardPage().successPopUP();
    }

    @Test(description = "Проверка поиска нового рабочего")
    public void searchNewEmployee() {
        pimpage().openPIM();
        pimpage().addBtnClick();
        pimpage().setFullName("Vladislav", "Neponyatnovich");
        dashboardPage().saveBtnClick();
        pimpage().openPIM();
        pimpage().setEployeeName("Vladislav Neponyatnovich");
        pimpage().searchBtnClick();
        pimpage().searchEmployeeAtRecords("Vladislav", "Neponyatnovich");
    }

    @Test(description = "Проверка поиска несуществующего поиска")
    public void searchUnexistingEmployee() {
        pimpage().openPIM();
        pimpage().setEployeeName("Kurtka Bein");
        pimpage().searchBtnClick();
        pimpage().checkPopUp();
    }
}
