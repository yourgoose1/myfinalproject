package ui.tests;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ui.pages.PageObjectSupplier;

public class BuzzPageTest extends BaseTest implements PageObjectSupplier {

    @BeforeClass
    public void login() {
        loginPage().loginByAdmin();
    }

    @Test(description = "Создание нового поста")
    public void createNewPost() {
        buzzpage().openBuzz();
        buzzpage().inputTextArea("Hello World!");
        buzzpage().postBtnClick();
        buzzpage().openBuzz();
        buzzpage().checkCreatedPost("Hello World!");
    }

    @Test(description = "Проверка лайка на новом посте")
    public void checkLikeOnPost() {
        createNewPost();
        buzzpage().like();
        buzzpage().checkLikes("1");
    }

    @Test(description = "Удаление нового поста")
    public void deletePostCheck() {
        createNewPost();
        buzzpage().dotsMenuClick();
        buzzpage().deletePostClick();
        buzzpage().confirmDelete();
        buzzpage().checkDeletedPost("Hello World!");
        Selenide.sleep(3000);
    }
}

