package ui.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class PimPage implements PageObjectSupplier {

    SelenideElement addBtn = $x("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']");
    SelenideElement firstNameField = $x("//input[@name='firstName']");
    SelenideElement lastnameField = $x("//input[@name='lastName']");
    SelenideElement searchBtn = $x("//button[@type='submit']");
    SelenideElement employeeNameField = $x("//input[@placeholder[1]='Type for hints...']");
    SelenideElement infoPopUp = $x("//div[@class='oxd-toast-container oxd-toast-container--bottom']");
    String nameAtFoundRecordsXpathTemplate = "//div[@role='cell']//div[contains(text(), '%s')]";

    public void openPIM() {
        dashboardPage().setSideMenuItem("PIM", "PIM");
    }

    public void addBtnClick() {
        addBtn.click();
    }

    public void setFullName(String firstName, String lastName) {
        firstNameField.sendKeys(firstName);
        lastnameField.sendKeys(lastName);
    }

    public void searchBtnClick() {
        searchBtn.click();
    }

    public void setEployeeName(String name) {
        employeeNameField.sendKeys(name);
    }

    public void searchEmployeeAtRecords(String firstName, String lastName) {
        $x(String.format(nameAtFoundRecordsXpathTemplate, firstName)).shouldBe(visible);
        $x(String.format(nameAtFoundRecordsXpathTemplate, lastName)).shouldBe(visible);
    }

    public void checkPopUp() {
        infoPopUp.shouldBe(visible);
    }
}
