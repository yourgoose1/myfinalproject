package ui.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$x;

public class DashboardPage {
    SelenideElement sidePanel = $x("//ul[@class='oxd-main-menu']");
    SelenideElement searchBar = $x("//input[@placeholder='Search']");
    SelenideElement configurationTool = $x("//i[@class='oxd-icon bi-gear-fill orangehrm-leave-card-icon']");
    SelenideElement checkBox = $x("//span[@class='oxd-switch-input oxd-switch-input--active --label-right']");
    SelenideElement saveBtn = $x("//button[@type='submit']");
    SelenideElement cancelBtn = $x("//button[@class='oxd-button oxd-button--medium oxd-button--ghost']");
    SelenideElement successPopUp = $x("//div[@id='oxd-toaster_1']");
    String topBarItemXpathTemplate = "//span[@class='oxd-topbar-header-breadcrumb']//h6[text()='%s']";

    String quickLaunchPanelXpathTemplate = "//button[@title='%s']";
    String sideMenuItemXpathTemplate = "//span[text()='%s']";

    public void checkSidePanel() {
        sidePanel.shouldBe(visible);
    }

    public void drilldownAtQuickPanel(String quickPanelBtn, String topBarItemName) {
        $x(String.format(quickLaunchPanelXpathTemplate, quickPanelBtn)).click();
        $x(String.format(topBarItemXpathTemplate, topBarItemName)).shouldBe(visible);
    }

    public void checkSearching(String userInput, String sideMenuItem) {
        searchBar.sendKeys(userInput);
        $x(String.format(sideMenuItemXpathTemplate, sideMenuItem)).shouldBe(visible).shouldBe(interactable);
    }

    public void setSideMenuItem(String sideMenuItem, String topBarItemName) {
        $x(String.format(sideMenuItemXpathTemplate, sideMenuItem)).click();
        $x(String.format(topBarItemXpathTemplate, topBarItemName)).shouldBe(visible);
    }

    public void configToolClick() {
        configurationTool.click();
    }

    public void checkBoxClick() {
        checkBox.click();
    }

    public void saveBtnClick() {
        saveBtn.click();
    }

    public void cancelBtnClick() {
        cancelBtn.click();
    }

    public void successPopUP() {
        successPopUp.shouldBe(visible);
    }

    public void successPopUpHidden() {
        successPopUp.shouldBe(hidden);
    }
}
