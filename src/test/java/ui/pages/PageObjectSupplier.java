package ui.pages;

public interface PageObjectSupplier {
    default LoginPage loginPage() {
        return new LoginPage();
    }

    default DashboardPage dashboardPage() {
        return new DashboardPage();
    }

    default PimPage pimpage() {
        return new PimPage();
    }

    default BuzzPage buzzpage() {
        return new BuzzPage();
    }
}
