package ui.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;
import static ui.tests.SystemUser.ADMIN;

public class LoginPage {
    SelenideElement usernameInput = $x("//input[@name='username']");
    SelenideElement passwordInput = $x("//input[@name='password']");
    SelenideElement loginBtn = $x("//button[@type='submit']");
    SelenideElement invalidCredentialsAlert = $x("//div[@role='alert']");
    SelenideElement requiredLabelAlert = $x("//div[@class='oxd-input-group oxd-input-field-bottom-space']//span[text()='Required']");

    public void openWebsite() {
        Selenide.open("https://opensource-demo.orangehrmlive.com/");
    }

    public void authorization(String username, String password) {
        usernameInput.sendKeys(username);
        passwordInput.sendKeys(password);

    }

    public void clickLoginBtn() {
        loginBtn.click();
    }

    public void checkAlert() {
        invalidCredentialsAlert.shouldBe(visible);
    }

    public void checkRequiredLabelAlert() {
        requiredLabelAlert.shouldBe(visible);
    }

    public void loginByAdmin() {
        Selenide.open("https://opensource-demo.orangehrmlive.com/");
        authorization(ADMIN.username, ADMIN.password);
        loginBtn.click();
    }

}
