package ui.pages;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.hidden;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$x;

public class BuzzPage implements PageObjectSupplier {

    SelenideElement textareaField = $x("//textarea[@class='oxd-buzz-post-input']");
    SelenideElement postBtn = $x("//button[@type='submit']");
    SelenideElement likeBtn = $x("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[1]/div/div[3]/div[1]/div/div[3]/div[1]/div");
    SelenideElement dotsMenu = $x("//i[@class[1]='oxd-icon bi-three-dots']");
    SelenideElement deletePostBtn = $x("//p[text()='Delete Post']");
    SelenideElement confirmDeleteBtn = $x("//button[text()=' Yes, Delete ']");
    String likesNumberXpathTemplate = "//p[text()='1 Like']";
    String newPostTextXpathTemplate = "//p[text()='%s']";

    public void openBuzz() {
        dashboardPage().setSideMenuItem("Buzz", "Buzz");
    }

    public void inputTextArea(String input) {
        textareaField.sendKeys(input);
    }

    public void postBtnClick() {
        postBtn.click();
    }

    public void checkCreatedPost(String postText) {
        $x(String.format(newPostTextXpathTemplate, postText)).shouldBe(visible);
    }

    public void checkDeletedPost(String postText) {
        $x(String.format(newPostTextXpathTemplate, postText)).shouldBe(hidden);
    }

    public void like() {
        likeBtn.click();
    }

    public void checkLikes(String number) {
        $x(String.format(likesNumberXpathTemplate, number)).shouldBe(visible);
    }

    public void dotsMenuClick() {
        dotsMenu.click();
    }

    public void deletePostClick() {
        deletePostBtn.click();
    }

    public void confirmDelete() {
        confirmDeleteBtn.click();
    }
}
