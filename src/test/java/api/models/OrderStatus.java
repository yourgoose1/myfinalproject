package api.models;

import java.util.Random;

public enum OrderStatus {
    PLACED("placed"),
    APPROVED("approved"),
    DELIVERED("delivered");

    OrderStatus(String orderStatusName) {
        this.orderStatusName = orderStatusName;
    }

    public String orderStatusName;

    private static final Random randomOrderStatus = new Random();

    public static OrderStatus randomOrderStatus() {
        OrderStatus[] orderStatuses = values();
        return orderStatuses[randomOrderStatus.nextInt(orderStatuses.length)];
    }
}
