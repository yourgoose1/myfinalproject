package api.models;

import java.util.Random;

public enum Status {
    AVAILABLE("available"),
    PENDING("pending"),
    SOLD("sold");

    Status(String statusName) {
        this.statusName = statusName;
    }

    public String statusName;

    private static final Random randomStatus = new Random();

    public static Status randomStatus() {
        Status[] statuses = values();
        return statuses[randomStatus.nextInt(statuses.length)];
    }
}
