package api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Store {
    public int id;
    public long petId;
    public int quantity;
    public String shipDate;
    public String status;
    public boolean complete;

}
