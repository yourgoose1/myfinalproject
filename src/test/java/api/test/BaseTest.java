package api.test;

import api.models.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import com.github.javafaker.Faker;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class BaseTest {
    static Faker faker = new Faker();
    static String fakeUserName = faker.name().username();
    static Random random = new Random();
    static int randomId = random.nextInt(10);

    static ObjectMapper objectMapper = new ObjectMapper();

    public static String baseUri = "https://petstore.swagger.io/v2";
    public static File newImage = new File("src/test/resources/newImage.jpg");

    public static RequestSpecification getLogSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseUri)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }

    public static ResponseSpecification responseLogSpec() {
        return new ResponseSpecBuilder()
                .log(LogDetail.ALL)
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();
    }

    public static RequestSpecification sendImageSpec() {
        return new RequestSpecBuilder()
                .setBaseUri(baseUri)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.MULTIPART)
                .log(LogDetail.ALL)
                .build();
    }


    public static String newPetAllFieldsJson() {
        Faker faker = new Faker();

        Pet pet = new Pet();
        pet.setId(randomId);
        pet.setCategory(new Category(random.nextInt(1, 30), "Dog"));
        pet.setName(faker.dog().name());
        pet.setPhotoUrls(new ArrayList<>(List.of(faker.avatar().image())));
        pet.setTags(new ArrayList<>(List.of(new Tag(random.nextInt(1, 30), faker.dog().breed()))));
        pet.setStatus(Status.randomStatus().statusName);
        try {
            return objectMapper.writeValueAsString(pet);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }

    public static String newPetWithoutAllFieldsJson() {
        Faker faker = new Faker();

        Pet pet2 = new Pet();
        pet2.setName("Hachi");
        pet2.setPhotoUrls(new ArrayList<>(List.of(faker.avatar().image())));
        return pet2.toString();
    }

    public static String newUserAllFieldsJson() {
        Random random = new Random();
        Faker faker = new Faker();
        User user = new User();
        user.setId(random.nextInt());
        user.setUsername(fakeUserName);
        user.setFirstName(faker.name().firstName());
        user.setLastName(faker.name().lastName());
        user.setEmail(faker.internet().emailAddress());
        user.setPassword(faker.internet().password());
        user.setPhone(faker.phoneNumber().phoneNumber());
        user.setUserStatus(random.nextInt());
        try {
            return objectMapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String newOrderJson() {
        Random random = new Random();
        Store store = new Store();
        store.setId(randomId);
        store.setPetId(randomId);
        store.setQuantity(random.nextInt(10));
        store.setShipDate("2024-01-22T00:00:00.000+0000");
        store.setStatus(OrderStatus.randomOrderStatus().orderStatusName);
        store.setComplete(random.nextBoolean());
        try {
            return objectMapper.writeValueAsString(store);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String newOrderNonValidDateJson() {
        Random random = new Random();
        Store store = new Store();
        store.setId(random.nextInt(10));
        store.setPetId(randomId);
        store.setQuantity(random.nextInt(10));
        store.setShipDate("21 сентября");
        store.setStatus(OrderStatus.randomOrderStatus().orderStatusName);
        store.setComplete(random.nextBoolean());
        try {
            return objectMapper.writeValueAsString(store);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}

