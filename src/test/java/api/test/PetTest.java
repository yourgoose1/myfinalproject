package api.test;

import api.models.Pet;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PetTest extends BaseTest {

    @Test(description = "Создание нового питомца, заполнив все поля")
    public void createNewPetAllFields() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .body(BaseTest.newPetAllFieldsJson())
                .when()
                .post("/pet")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Создание питомца не заполнив все поля")
    public void createNewPetWithoutAllFields() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .body(BaseTest.newPetWithoutAllFieldsJson())
                .when()
                .post("/pet")
                .then()
                .statusCode(400)
                .body("message", Matchers.equalTo("bad input"));
    }

    @Test(description = "Получение информации о питомце")
    public void getPet() {
        createNewPetAllFields();
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("petId", randomId)
                .when()
                .get("/pet/{petId}")
                .then()
                .spec(responseLogSpec())
                .extract();
    }

    @Test(description = "Получение информации о несуществующем питомце")
    public void getUnexistingPet() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("petId", 98767)
                .when()
                .get("/pet/{petId}")
                .then()
                .statusCode(404);
    }

    @Test(description = "Изменение имени питомца")
    public void changePetNameById() {
        createNewPetAllFields();
        RestAssured
                .given()
                .spec(getLogSpec())
                .contentType(ContentType.URLENC)
                .formParam("name", faker.dog().name())
                .pathParam("petId", randomId)
                .when()
                .post("/pet/{petId}")
                .then()
                .spec(responseLogSpec())
                .body("message", Matchers.equalTo(String.valueOf(randomId)));
    }

    @Test(description = "Удаление существующего питомца")
    public void deleteCreatedPet() {
        createNewPetAllFields();
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("petId", randomId)
                .when()
                .delete("/pet/{petId}")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Удаление несуществующего питомца")
    public void deleteUnexistingPet() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .when()
                .delete("/pet/987")
                .then()
                .statusCode(404);
    }

    @Test(description = "Проверка фильтрации по статусу 'Sold'")
    public void assertGetSoldPet() {
        boolean soldPet = RestAssured
                .given()
                .spec(getLogSpec())
                .queryParams("status", "sold")
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(responseLogSpec())
                .extract()
                .body()
                .jsonPath()
                .getList("$", Pet.class)
                .stream()
                .allMatch(pet -> pet.status.equals("sold"));

        Assert.assertTrue(soldPet);
    }

    @Test(description = "Фильтрация по несуществующему статусу")
    public void getUnexistingStatusPet() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .queryParams("status", "cooked")
                .when()
                .get("/pet/findByStatus")
                .then()
                .spec(responseLogSpec())
                .body("message", Matchers.empty());
    }

    @Test(description = "Загрузка изображения")
    public void uploadNewImageByPetId() {
        RestAssured
                .given()
                .spec(sendImageSpec())
                .pathParam("petId", randomId)
                .formParam("additionalMetadata", "test")
                .multiPart("file", newImage, "image/jpeg")
                .when()
                .post("/pet/{petId}/uploadImage")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }
}
