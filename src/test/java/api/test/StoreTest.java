package api.test;

import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class StoreTest extends BaseTest {

    @Test(description = "Создание нового заказа")
    public void addOrder() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .body(newOrderJson())
                .when()
                .post("/store/order")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Добавление заказа с невалидной датой")
    public void addOrderWithNonValidDate() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .body(newOrderNonValidDateJson())
                .when()
                .post("/store/order")
                .then()
                .statusCode(500);
    }

    @Test(description = "Удаление заказа")
    public void deleteOrder() {
        addOrder();
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("orderId", randomId)
                .when()
                .delete("/store/order/{orderId}")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Удаление несуществующего заказа")
    public void deleteUnexistentOrder() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .when()
                .delete("/store/order/9827")
                .then()
                .statusCode(404);
    }

    @Test(description = "Поиск заказа по идентификатору")
    public void findOrder() {
        addOrder();
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("orderId", randomId)
                .when()
                .get("/store/order/{orderId}")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Поиск несуществующего заказа")
    public void findUnexistentOrder() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("orderId", "11")
                .when()
                .get("/store/order/{orderId}")
                .then()
                .statusCode(404);
    }
}


