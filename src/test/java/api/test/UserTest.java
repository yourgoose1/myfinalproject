package api.test;

import io.restassured.RestAssured;
import org.testng.annotations.Test;

public class UserTest extends BaseTest {
    @Test(description = "Создание нового пользователя")
    public void createNewUser() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .body(BaseTest.newUserAllFieldsJson())
                .when()
                .post("/user")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Получение информации о новом пользователе по имени пользователя")
    public void getUserByUserName() {
        createNewUser();
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("username", fakeUserName)
                .when()
                .get("/user/{username}")
                .then()
                .spec(responseLogSpec())
                .extract();
    }

    @Test(description = "Получение информации о несуществующем пользователе по имени пользователя")
    public void getUnexistingUserByUserName() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("username", "PRO100MASTER")
                .when()
                .get("/user/{username}")
                .then()
                .statusCode(404);
    }

    @Test(description = "Удаление существующего пользователя")
    public void deleteUser() {
        createNewUser();
        RestAssured
                .given()
                .spec(getLogSpec())
                .pathParam("username", fakeUserName)
                .when()
                .delete("/user/{username}")
                .then()
                .spec(responseLogSpec())
                .extract()
                .response();
    }

    @Test(description = "Удаление несуществующего пользователя")
    public void deleteUnexistingUser() {
        RestAssured
                .given()
                .spec(getLogSpec())
                .when()
                .delete("/user/Super_CoOl")
                .then()
                .statusCode(404);
    }
}
